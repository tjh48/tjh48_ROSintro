# Reset TurtleSim window
rosservice call /reset

# Write the letter 'T' with turtle1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

