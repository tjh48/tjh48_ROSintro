# Reset TurtleSim window
rosservice call /reset

# Spawn turtle2
rosservice call /spawn '6.0' '5.0' '0.0' 'turtle2'

# Teleport turtle1 to starting location
rosservice call /turtle1/teleport_absolute '3.0' '5.0' '0.0'

# Set pen colors
rosservice call /turtle1/set_pen '240' '0' '0' '3' '0'
rosservice call /turtle2/set_pen '0' '204' '0' '3' '0'

# Clear background to prepare for letters
rosservice call /clear

# Write the letter 'T' with turtle1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

# Write the letter 'H' with turtle2
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, -2.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[2.0, 1.0, 0.0]' '[0.0, 0.0, -2.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.6, 0.3, 0.0]' '[0.0, 0.0, 0.0]'

